#pragma once

#include "ArmorComponent.h"
#include "ArmorComponent.generated.h"

UCLASS()
class UArmorAdapter : UHealthInterface
{
	GENERATED_BODY()

public:
	void GetArmorComponent();
	void ReducePlayerHealth() override;

private:
	UPROPERTY()
		UArmorComponent* Armor { nullptr };
};

// returning an instance of the armor class
inline UArmorComponent* UArmorAdapter::GetArmorComponent()
{
	if (!Armor)
	{
		Armor = dynamic_cast<UArmorComponent*>(GetOwner()->GetComponentByClass(UArmorComponent::StaticClass()));
	}
	return Armor;
}

// adapting the health interface for the armor class
inline void UArmorAdapter::ReducePlayerHealth()
{
	Armor.ReducePlayerArmor();
}