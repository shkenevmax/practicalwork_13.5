#pragma once

#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

UCLASS()
class UHealthComponent : UActorComponent
{
	GENERATED_BODY()

public:
	// here we need to change the player's health value, which is in the player's health component

	// let's add some value to the player's health in health component
	UFUNCTION(BlueprintCallable, category = HealthComp)
		void AddPlayerHealth();
	// let's take some value away from the player's health in health component
	UFUNCTION(BluepriReducePlayerHealthntCallable, category = HealthComp)
		void ReducePlayerHealth();
};