#pragma once

#include "UObject/Object.h"
#include "Cell.generated.h"

UCLASS()
class UCell : public UObject
{
	GENERATED_BODY()

public:
	virtual bool GetIsCanTake() const;
	virtual bool GetIsEmpty() const;
	virtual void Take();
	virtual void Put();

private:
	bool bIsCanTake { false };
	bool bIsEmpty { false };
};

inline bool UCell::GetIsCanTake()
{
	return bIsCanTake;
}

inline bool UCell::GetIsEmpty()
{
	return bIsEmpty;
}

inline void UCell::Take()
{
	bIsEmpty = true;
}

inline void UCell::Put()
{
	bIsEmpty = false;
}