#pragma once

#include "�hest�ompartment.h"
#include "Chest.generated.h"

UCLASS()
class UChest : public UObject
{
	GENERATED_BODY()

public:
	void BuildChest(const FString& Scheme);

protected:
	U�hest�ompartment* Parse�hest�ompartment();
	UCell* ParseCell();
	void ParseScheme(const FString& Scheme);

private:
	UPROPERTY()
		U�hest�ompartment* �hest�ompartmentRoot { nullptr };
};

inline U�hest�ompartment* UChest::Parse�hest�ompartment()
{
	return NewObject<U�hest�ompartment>(this);
}

inline UCell* UChest::ParseCell()
{
	return NewObject<UCell>(this);
}

inline void UChest::ParseScheme(const FString& Scheme)
{
	ParseScheme(Scheme);
}

TArray<U�hest�ompartment> �hest�ompartments;
FString Storage;

for (auto Symbol : Scheme)
{
	if (Symbol == '}')
	{
		�hest�ompartments.Pop();
	}
	else
	{
		if (Symbol == ':')
		{
			auto �hest�ompartment = Parse�hest�ompartment();
			if (�hest�ompartments.Num == 0)
			{
				�hest�ompartmentsRoot = �hest�ompartment;
			}
			else
			{
				�hest�ompartments.Top->AddCell(�hest�ompartment);
			}
			�hest�ompartments.Push(�hest�ompartment);
		}
		else
		{
			Storage += Symbol;
			if (Storage.Contains("Cell", ESearchCase::IgnoreCase))
			{
				auto Cell = ParseCell();
				�hest�ompartments.Top()->AddCell(Cell);
				Storage = "";
			}
		}
	}
}