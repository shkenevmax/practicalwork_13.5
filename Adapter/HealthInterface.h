#pragma once

#include "IHealth.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UHealthInterface : public UInterface
{
    GENERATED_BODY()

public:
    // fully virtual functions
    virtual void AddPlayerHealth() = 0;
    virtual void ReducePlayerHealth() = 0;
};