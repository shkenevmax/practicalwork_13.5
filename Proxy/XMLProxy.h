#pragma once

#include "GameGlossaryBase.h"

class GameGlossaryBase;

class GlossaryBaseProxy final
{
	using cStr = const FString;
	using BaseMap = TMap<cStr, GameGlossaryBase>;

public:
	int GetChapterIndex(cStr& Base, cStr& Name);
	FString GetChapterDescription(cStr& Base, cStr& Name);
	bool GetIsAlreadyRead(cStr& Base, cStr& Name);
	bool SetIsAlreadyRead(cStr& Base, cStr& Name, bool Value);

protected:
	void ParseBase(cStr& Name);

private:
	BaseMap DataBase;
};

inline int GlossaryBaseProxy::GetChapterIndex(cStr& Base, cStr& Name)
{
	if (!DataBase.Contains(Base)) ParseDoc(Base);
	return DataBase[Base].GetChapterIndex(TCHAR_TO_ANSI(*Name));
}

inline FString GlossaryBaseProxy::GetChapterDescription(cStr& Base, cStr& Name)
{
	if (!DataBase.Contains(Base)) ParseDoc(Base);
	return DataBase[Base].GetChapterDescription(TCHAR_TO_ANSI(*Name));
}

inline bool GlossaryBaseProxy::GetIsAlreadyRead(cStr& Base, cStr& Name)
{
	if (!DataBase.Contains(Base)) ParseDoc(Base);
	return DataBase[Base].GetIsAlreadyRead(TCHAR_TO_ANSI(*Name));
}

inline bool GlossaryBaseProxy::SetIsAlreadyRead(cStr& Base, cStr& Name, bool Value)
{
	if (!DataBase.Contains(Base)) ParseDoc(Base);
	return DataBase[Base].SetIsAlreadyRead(TCHAR_TO_ANSI(*Name), Value);
}

inline void GlossaryBaseProxy::ParseBase(cStr& Name)
{
	const auto Base = GameGlossaryBase::ParseBase(TCHAR_TO_ANSI(*Name));
	if (Base.IsValid())
	{
		DataBase.Add(Name, Base);
	}
}