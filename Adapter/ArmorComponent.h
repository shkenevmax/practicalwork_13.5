#pragma once

#include "Components/ActorComponent.h"
#include "ArmorComponent.generated.h"

UCLASS()
class UArmorComponent : UActorComponent, UArmorAdapter
{
	GENERATED_BODY()

public:
	// the simplest functions of increasing and decreasing armor
	void AddPlayerArmor();
	void ReducePlayerArmor();
};