#pragma once

#include "Cell.h"
#include"�hest�ompartment.generated.h"

UCLASS()
class U�hest�ompartment : public UCell
{
	GENERATED_BODY()

public:
	virtual bool GetIsCanTake() const override;
	virtual bool GetIsEmpty() const override;
	virtual void Take() override;
	virtual void Put() override;

private:
	UPROPERTY()
		TArray<UCell> Cells;
};

inline bool U�hest�ompartment::GetIsCanTake() const
{
	for (auto Cell : Cells)
	{
		if (Cell->GetIsCanTake()) return true;
		return false;
	}
}

inline bool U�hest�ompartment::GetIsEmpty() const
{
	bool bResult{ true };
	for (auto Cell : Cells)
	{
		bResult *= Cell->GetIsEmpty();
	}
	return bResult;
}

inline void U�hest�ompartment::Take()
{
	for (auto Cell : Cells)
	{
		if (!Cell->GetIsCanTake())
		{
			return Cell->Take();
		}
	}
}

inline void U�hest�ompartment::Put()
{
	for (auto Cell : Cells)
	{
		Cell->Put();
	}
}