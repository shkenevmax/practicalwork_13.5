#pragma once

#include "Blueprint/UserWidget.h"
#include "WidgetTree.h"
#include "TextWidgetTypes.h"
#include "TextBlock.h"
#include "MyWidget.generated.h"

UCLASS()
class UMyUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	// here we need to change the player's health value that is displayed on the screen

	UPROPERTY()
		UTextBlock* HealthCount;
	// let's add some value to the player's health in player's widget
	UFUNCTION()
		void ReduceWidgetHealth(BlueprintCallable, category = Health);
	// let's take some value away from the player's health in player's widget
	UFUNCTION()
		void AddWidgetHealth(BlueprintCallable, category = Health);
};