#pragma once


#include "activation.h"

#include "Components/ActorComponent.h"
#include "HealthComponent.h"
#include "MyWidget.h"
#include "HealthTextFacade.generated.h"

class UHealthComponent;
class UMyWidget;

UCLASS()
class UHealthTextFacade : public UActorComponent
{
	GENERATED_BODY()

	using HealthComp = UHealthComponent;
	using MyWidget = UMyWidget;

public:
	UFUNCTION(BlueprintCallable, category = HealthChange)
		virtual void ReduceHealth(AActor* Target);
	UFUNCTION(BlueprintCallable, category = HealthChange)
		virtual void AddHealth(AActor* Target);

	// we will take away a certain amount of health at once in the value on the screen and in the health component
	inline void UHealthTextFacade::ReduceHealth(AActor* Target)
	{
		auto Health = dynamic_cast<HealthComp>(GetOwner()->GetComponentByClass(HealthComp::StaticClass()));
		if (Health) Health->ReducePlayerHealth();
		auto Widget = dynamic_cast<MyWidget>(GetOwner()->GetComponentByClass(MyWidget::StaticClass()));
		if (Widget) Widget->ReduceWidgetHealth();
	}

	// let's add a certain amount of health at once in the value on the screen and in the health component
	inline void UHealthTextFacade::AddHealth(AActor* Target)
	{
		auto Health = dynamic_cast<HealthComp>(GetOwner()->GetComponentByClass(HealthComp::StaticClass()));
		if (Health) Health->AddPlayerHealth();
		auto Widget = dynamic_cast<MyWidget>(GetOwner()->GetComponentByClass(MyWidget::StaticClass()));
		if (Widget) Widget->AddWidgetHealth();
	}
};