#pragma once

class GameGlossaryBase final
{
public:
	static GameGlossaryBase ParseBase(const char* Name);
	bool IsValid() const;
	// read the index of the chapter from the database
	int GetChapterIndex(const char* Name) const;
	// read the contents of the chapter from the database
	FString GetChapterDescription(const char* Name) const;
	// let's find out if the chapter has been read
	bool GetIsAlreadyRead(const char* Name) const;
	// setting the value for the chapter "read"
	bool SetIsAlreadyRead(const char* Name) const;
};