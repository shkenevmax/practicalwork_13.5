#pragma once

#include "Components/ActorComponent.h"
#include "IKHandlingComponent.generated.h"

class USkeletalMeshSocket;

UCLASS()
class UIKHandlingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USkeletalMeshSocket* GetInteractionSocket() const;
	void UseSocketTransformBegin(USkeletalMeshSocket* TargetSocket);
	void UseSocketTransformEnd(USkeletalMeshSocket* TargetSocket);
};